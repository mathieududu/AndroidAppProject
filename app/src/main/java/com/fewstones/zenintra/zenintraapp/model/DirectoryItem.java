package com.fewstones.zenintra.zenintraapp.model;

/**
 * Created by mathieuduverney on 02/09/2016.
 */

public class    DirectoryItem {


    private String img_directory;
    private String name_directory;


    public DirectoryItem(String img_directory, String name_directory){

        this.img_directory=img_directory;
        this.name_directory=name_directory;



    }


    public String getImg_directory() {
        return img_directory;
    }

    public String getName_directory() {
        return name_directory;
    }



    public void setName_directory(String name_directory) {
        this.name_directory = name_directory;
    }

    public void setImg_directory(String img_directory) {
        this.img_directory = img_directory;
    }


}
