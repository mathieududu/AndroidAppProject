package com.fewstones.zenintra.zenintraapp.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathieuduverney on 22/06/2016.
 */

public class ListChatRequest extends StringRequest {



    public final static String ListChat_Request_URL = "https://zenintra.net/chat/chats";
    private HashMap<String,String> params;



    public ListChatRequest(String token_dudu, Response.Listener listener, Response.ErrorListener errorListener){


        super(Method.POST, ListChat_Request_URL, listener, errorListener);

        params = new HashMap<>();
        params.put("jwt",token_dudu);

    }

    public Map<String,String> getParams(){

        return params;

    }



}

