package com.fewstones.zenintra.zenintraapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.fewstones.zenintra.zenintraapp.GCM.GCMRegistrationIntentService;
import com.fewstones.zenintra.zenintraapp.adapter.SlidingMenuAdapter;
import com.fewstones.zenintra.zenintraapp.fragment.fragment_directories;
import com.fewstones.zenintra.zenintraapp.fragment.fragment_listChat;
import com.fewstones.zenintra.zenintraapp.fragment.fragment_newMessage;
import com.fewstones.zenintra.zenintraapp.model.ItemSlideMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.ArrayList;
import java.util.List;


public class UserArea extends AppCompatActivity{



    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String token;


    private List<ItemSlideMenu> listSliding;
    private SlidingMenuAdapter adapter;
    private ListView listViewSliding;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_area);


       Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        toolbar.setTitleTextColor(Color.WHITE);







        //////////////////////////////////////////////////////////////////////////////////////////


        // Navigation Drawer


        //Init component
        listViewSliding = (ListView) findViewById(R.id.lv_drawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        listSliding = new ArrayList<>();

        //Add item sliding list
        listSliding.add(new ItemSlideMenu(R.drawable.ic_question_answer_black_48dp, "List"));
        listSliding.add(new ItemSlideMenu(R.drawable.ic_supervisor_account_black_48dp, "Directory"));


        adapter = new SlidingMenuAdapter(this, listSliding);
        listViewSliding.setAdapter(adapter);

        //Display icon to open/close list
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Set title
        setTitle(listSliding.get(0).getTitle());

        //item selected
        listViewSliding.setItemChecked(0, true);

        drawerLayout.closeDrawer(listViewSliding);

        //Display fragment
        replaceFragment(0);

        //Handle on item click

        listViewSliding.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //set title
                setTitle(listSliding.get(position).getTitle());

                //item selected
                listViewSliding.setItemChecked(position, true);

                //Replace fragment
                replaceFragment(position);


                //close menu
                drawerLayout.closeDrawer(listViewSliding);

            }
        });

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_opened, R.string.drawer_closed) {


            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);


        ////////////////////////////////////////////////////////////////////////////////////////////




        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //check type of intent filter

                if(intent.getAction().endsWith(GCMRegistrationIntentService.REGISTRATION_SUCCESS)){

                    token = intent.getStringExtra("token");
                 //   Toast.makeText(getApplicationContext(), "GCM token:"+token, Toast.LENGTH_SHORT).show();

                }
                else if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)){

                    Toast.makeText(getApplicationContext(),"GCM error",Toast.LENGTH_SHORT).show();
                }
            }
        };

        //check staatus of google play service in device

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if(ConnectionResult.SUCCESS != resultCode){


            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)){

                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enable in this device", Toast.LENGTH_SHORT).show();
                //So notification
                GooglePlayServicesUtil.showErrorNotification(resultCode,getApplicationContext());

            }else {

                Toast.makeText(getApplicationContext(), "This device doesn't support for Google Play Service", Toast.LENGTH_SHORT).show();

            }

        }else {

            //Start Service
            Intent intent = new Intent(this, GCMRegistrationIntentService.class);
            startService(intent);


        }


    }

    @Override
    protected void onResume(){

        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    }

    @Override
    protected void onPause(){

        super.onPause();


        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }


    @Override
    public void onBackPressed(){

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);
    }


    public String getToken(){

        return token;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){

            return true;

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();

    }

    //create methode fragment
    private  void  replaceFragment(int pos){

        Fragment fragment=null;

        switch (pos){

            case 0:
                fragment=new fragment_listChat();
                break;

            case 1:
                fragment=new fragment_directories();
                        break;

            default:
                fragment=new fragment_listChat();
                break;


        }

        if(null!=fragment){


            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_content,fragment);
            transaction.addToBackStack(null);
            transaction.commit();

        }

    }









}


