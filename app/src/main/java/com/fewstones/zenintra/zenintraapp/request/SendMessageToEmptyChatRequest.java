package com.fewstones.zenintra.zenintraapp.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathieuduverney on 29/06/2016.
 */

public class SendMessageToEmptyChatRequest extends StringRequest {



    private static final String LOGIN_REQUEST_URL = "https://zenintra.net/chat/send_message";
    HashMap<String,String> params = new HashMap<>();




    public SendMessageToEmptyChatRequest(String token_dudu, String MESSAGE_EDIT, ArrayList<Integer> users, String chatName, Response.Listener<String> listener, Response.ErrorListener errorListener){


        super(Method.POST,LOGIN_REQUEST_URL,listener,errorListener);

        params = new HashMap<>();

        try {

            // ------------- in users array -----------


            JSONArray jsonArray_users = new JSONArray();




            // create JSON object id


            for(int i=0; i<users.size();i++){

                JSONObject id_js = new JSONObject();

                ArrayList<JSONObject> id = new ArrayList<>();
                id.add(id_js);

               id_js.put("id",users.get(i));

                jsonArray_users.put(id_js);
            }




            //// --------- in active_chat object ----------

            JSONObject jsonObject_one = new JSONObject();


            jsonObject_one.put("users",jsonArray_users);

            jsonObject_one.put("chat_type","empty");

            if (users.size()>2 && !chatName.equals("one-to-one")) {

                jsonObject_one.put("chat_name", chatName);
                jsonObject_one.put("chat_intent", "group");
            }
            else {
                jsonObject_one.put("chat_intent", "one-to-one");
            }



            // ------------- Root object ---------


            params.put("active_chat",jsonObject_one.toString(13)); //insert jsonObject_One in active_chat  // 13 for pretty
            params.put("text",MESSAGE_EDIT);
            params.put("jwt",token_dudu);



        }

        catch (JSONException e) {
            e.printStackTrace();

        }

    }

    @Override
    public Map<String,String> getParams() {

        return params;

    }

}

