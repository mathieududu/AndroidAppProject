package com.fewstones.zenintra.zenintraapp.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fewstones.zenintra.zenintraapp.model.DirectoryItem;
import com.fewstones.zenintra.zenintraapp.R;
import com.fewstones.zenintra.zenintraapp.View_User;
import com.fewstones.zenintra.zenintraapp.adapter.DirectoriesAdapter;
import com.fewstones.zenintra.zenintraapp.request.DirectoryRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class fragment_directories extends ListFragment implements AdapterView.OnItemClickListener{


    private List<DirectoryItem> directoryItems;
    private DirectoriesAdapter adapter;
    Context context;

    private ArrayList<String> first_name = new ArrayList<>();
    private ArrayList<String> last_name = new ArrayList<>();
    private ArrayList<String> name= new ArrayList<>();
    private ArrayList<String> photo_icon=new ArrayList<>();
    private ArrayList<String> phone_number = new ArrayList<>();
    private ArrayList<String> email = new ArrayList<>();
    private ArrayList<String> id_checked = new ArrayList<>();


    String token_dudu = "No token";
    int company_id;
    int my_id;

    ImageView call;
    ImageView mail;
    ImageView view;
    ListView lv;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {

        context=getActivity();
        View rootview = inflater.inflate(R.layout.activity_fragment_directories,container ,false);
        call = (ImageView) rootview.findViewById(R.id.icon_call);
        mail = (ImageView) rootview.findViewById(R.id.icon_mail);
        view = (ImageView) rootview.findViewById(R.id.icon_view);

        lv = (ListView) rootview.findViewById(android.R.id.list);

        return  rootview;



    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {


        super.onActivityCreated(savedInstanceState);






        ////////////////////////////////// GET MY TOKEN ////////////////////////////////////////////////

        SharedPreferences getSharedPreferences= context.getSharedPreferences("MyPref",0);
        token_dudu = getSharedPreferences.getString("sharedToken", "No Token");
        my_id = getSharedPreferences.getInt("user_id", 0);
        company_id = getSharedPreferences.getInt("company_id", 0);




        ////////////////////////////////////////////////////////////////////////////////////////////////



        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    directoryItems=new ArrayList<DirectoryItem>();

                    JSONArray jsonArray = new JSONArray(response);
                    int number = jsonArray.length();



                    for (int i = 0; i < number; i++) {


                        JSONObject jsonObject = jsonArray.getJSONObject(i);


                        String first_name_string = jsonObject.getString("first_name");
                        String last_name_string = jsonObject.getString("last_name");

                        first_name.add(first_name_string);
                        last_name.add(last_name_string);

                        //get full name
                        name.add(first_name.get(i) + " " + last_name.get(i));


                        //get photo
                        String photo = jsonObject.getString("photo");

                        if (photo.equals("null")) {
                            photo_icon.add("https://zenintra.net/img/defaultIcon.png");
                        } else {
                            photo_icon.add(jsonObject.getString("photo"));
                        }


                        //get phone number
                        String phone_number_user= jsonObject.getString("phone_number");
                        phone_number.add(phone_number_user);

                        //get email
                        String email_user = jsonObject.getString("email");
                        email.add(email_user);

                        //get id_checked
                        String id_user = jsonObject.getString("id");
                        id_checked.add(id_user);


                    }



                    for (int j=0;j<number;j++){

                        //display in the listview
                        DirectoryItem directoryItem = new DirectoryItem(photo_icon.get(j), name.get(j));
                        directoryItems.add(directoryItem);

                    }

                    adapter = new DirectoriesAdapter(getActivity(),directoryItems,fragment_directories.this);
                    setListAdapter(adapter);







                }catch (JSONException e){

                    e.printStackTrace();

                }

            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        };

        //Request
        DirectoryRequest directoryRequestt = new DirectoryRequest("https://zenintra.net/user/getUsersInCompany/"+company_id+"?jwt="+token_dudu,response,errorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(directoryRequestt);

        getListView().setOnItemClickListener(this);





    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {



        Intent intent = new Intent(view.getContext(), View_User.class);
        intent.putExtra("first_name",first_name.get(position));
        intent.putExtra("last_name",last_name.get(position));
        intent.putExtra("photo",photo_icon.get(position));
        intent.putExtra("phone",phone_number.get(position));
        intent.putExtra("mail",email.get(position));

        startActivityForResult(intent, 0);



    }




    public void call_number(final int position){



        if(phone_number.get(position).equals("")){

            AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle);
            builder
                    .setMessage(" User didn't specify his phone number ")
                    .setNegativeButton("Cancel",null)
                    .create()
                    .show();

        }else{

            AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle);
            builder
                    .setMessage("Phone number : "+phone_number.get(position))
                    .setNegativeButton("Cancel", null)
                    .setPositiveButton("Call", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {



                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:"+phone_number.get(position)));
                            startActivity(intent);

                        }
                    })
                    .create()
                    .show();

        }




    }


    public void send_mail(final int position){





        if(email.get(position).equals("")){

            AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle);
            builder
                    .setMessage(" User didn't specify his phone number ")
                    .setNegativeButton("Cancel",null)
                    .create()
                    .show();

        }else{

            AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle);
            builder
                    .setMessage("email : "+email.get(position))
                    .setNegativeButton("Cancel",null)
                    .setPositiveButton("Send e-mail", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {



                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("message/rfc822");
                            intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email.get(position)});
                            startActivity(Intent.createChooser(intent, "Send Email"));


                        }
                    })
                    .create()
                    .show();

        }




    }


    public void open_info(final int position){

        Intent intent = new Intent(context, View_User.class);
        intent.putExtra("first_name",first_name.get(position));
        intent.putExtra("last_name",last_name.get(position));
        intent.putExtra("photo",photo_icon.get(position));
        intent.putExtra("phone",phone_number.get(position));
        intent.putExtra("mail",email.get(position));

        startActivityForResult(intent, 0);

    }







}



