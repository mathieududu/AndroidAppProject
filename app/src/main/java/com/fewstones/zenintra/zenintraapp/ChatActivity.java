package com.fewstones.zenintra.zenintraapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import java.util.ArrayList;

/**
 * Created by mathieuduverney on 24/06/2016.
 */

public class ChatActivity extends FragmentActivity {

    private  String chat_id="";
    private int loop;
    private String chat_name="";

    ArrayList<Integer> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        //Get id from fragment_listChat

        Intent intent = getIntent();
        chat_id = intent.getStringExtra("id");

        loop = intent.getIntExtra("loop",50);
        chat_name = intent.getStringExtra("NameOfGroup");
        users = intent.getIntegerArrayListExtra("usersChecked");








    }

    //get chat_id in the fragment
    public String getMyData() {
        return chat_id;
    }


    public String getChat_name(){
        return chat_name;
    }


    public ArrayList<Integer> getUsers() {
        return users;
    }

    // To "refresh" when back button pressed

    @Override
    public void onBackPressed(){

        Intent intent = new Intent(ChatActivity.this, UserArea.class);
        startActivity(intent);
    }






}


