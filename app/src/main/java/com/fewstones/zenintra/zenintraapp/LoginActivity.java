package com.fewstones.zenintra.zenintraapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fewstones.zenintra.zenintraapp.request.LoginRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class LoginActivity extends AppCompatActivity {


    public final static String sharedToken = "sharedToken";
    public final static String LoginActivity = "";




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);


        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        final Button bLogin = (Button) findViewById(R.id.bLogin);
        //final SharedPreferences preferencesToken = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        final SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode








        String token2 = sharedPreferences.getString(sharedToken,"NO TOKEN");

        if (!token2.equals("NO TOKEN")){

            Intent intent = new Intent(this, UserArea.class);
            startActivity(intent);



        }





        bLogin.setOnClickListener(new View.OnClickListener() {




            @Override
            public void onClick(View v) {


                final String username = etUsername.getText().toString().trim();   //get username from editText
                final String password = etPassword.getText().toString();   //get password from editText
                final int platform=2;





               Response.Listener<String> responseListener = new Response.Listener<String>() {
                   @Override
                    public void onResponse(String response) {


                        try {


                            JSONObject jsonObject= new JSONObject(response);



                            String token = jsonObject.getString("token"); //get tokken array
                            int user_id = jsonObject.getInt("user_id");
                            int company_id = jsonObject.getInt("company_id");





                            /////////////// Store token, compagny_id and user_id //////////////////////


                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.putString("sharedToken",token);
                            editor.putInt("user_id",user_id);
                            editor.putInt("company_id", company_id);
                            editor.commit();



                            //////////////////////////////////////////////////






                            //Open new Activity UserArea

                            Intent intent = new Intent(LoginActivity.this, UserArea.class); //new activity
                            LoginActivity.this.startActivity(intent);   //start new activity






                        } catch (JSONException e) {

                            //if username/passord wrong (=no token value)

                          AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this,R.style.MyAlertDialogStyle);
                            builder
                                    .setMessage(R.string.login_failed)
                                    .setNegativeButton(R.string.Retry,null)
                                    .create()
                                    .show();
                        }
                    }
               };


                Response.ErrorListener errorListener = new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {



                    }

                };



                LoginRequest loginRequest = new LoginRequest(username, password , platform, responseListener, errorListener); //new stringRequest
                RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                queue.add(loginRequest);  //add to queue

            }
        });
    }

    @Override
    public void onBackPressed(){

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);
    }

}
