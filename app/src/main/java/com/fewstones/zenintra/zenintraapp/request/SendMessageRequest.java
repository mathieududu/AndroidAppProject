package com.fewstones.zenintra.zenintraapp.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathieuduverney on 27/06/2016.
 */

public class SendMessageRequest extends StringRequest {



    private static final String LOGIN_REQUEST_URL = "https://zenintra.net/chat/send_message";
    HashMap<String,String> params = new HashMap<>();



    public SendMessageRequest (String token_dudu,String MESSAGE_EDIT, int ChatID, int user1, int user2, Response.Listener<String> listener, Response.ErrorListener errorListener){


        super(Method.POST,LOGIN_REQUEST_URL,listener,errorListener);

        params = new HashMap<>();

        try {

            // ------------- in users array -----------


            JSONArray jsonArray_users = new JSONArray();


            JSONObject id1 = new JSONObject();      //Create id[]
            id1.put("id",user1);

            JSONObject id2 = new JSONObject();
            id2.put("id",user2);


            jsonArray_users.put(id1);  //insert id in the user array
            jsonArray_users.put(id2);



            //// --------- in active_chat object ----------

            JSONObject jsonObject_one = new JSONObject();

            jsonObject_one.put("id",ChatID);
            jsonObject_one.put("users",jsonArray_users);
            jsonObject_one.put("chat_intent","group");
            jsonObject_one.put("chat_type","group");



            // ------------- Root object ---------


            params.put("active_chat",jsonObject_one.toString(13)); //insert jsonObject_One in active_chat
            params.put("text",MESSAGE_EDIT);
            params.put("jwt",token_dudu);

        }

        catch (JSONException e) {
            e.printStackTrace();

        }

    }

    @Override
    public Map<String,String> getParams() {

        return params;

    }

}