package com.fewstones.zenintra.zenintraapp.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathieuduverney on 07/07/2016.
 */

public class NewGroupRequest extends StringRequest {


    private HashMap<String, String> params;


    public NewGroupRequest (String url, Response.Listener<String> listener, Response.ErrorListener errorListener){


        super(Method.GET,url,listener,errorListener);


    }


    @Override
    public Map<String,String> getParams() {
        return params;

    }



}
