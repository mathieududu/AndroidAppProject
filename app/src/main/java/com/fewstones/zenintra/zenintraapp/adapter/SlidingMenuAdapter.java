package com.fewstones.zenintra.zenintraapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fewstones.zenintra.zenintraapp.R;
import com.fewstones.zenintra.zenintraapp.model.ItemSlideMenu;

import java.util.List;

/**
 * Created by mathieuduverney on 01/09/2016.
 */

public class SlidingMenuAdapter extends BaseAdapter {

    private Context ctx;
    private List<ItemSlideMenu> listItem;

    public SlidingMenuAdapter(Context ctx, List<ItemSlideMenu> listItem) {
        this.ctx = ctx;
        this.listItem = listItem;
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

       View v=View.inflate(ctx, R.layout.item_slinding_menu,null);

        ImageView img_menu_drawer=(ImageView) v.findViewById(R.id.item_img);
        TextView txt_menu_drawer = (TextView) v.findViewById(R.id.item_title);

        ItemSlideMenu item = listItem.get(position);
        img_menu_drawer.setImageResource(item.getImgID());
        txt_menu_drawer.setText(item.getTitle());



        return v;
    }
}
