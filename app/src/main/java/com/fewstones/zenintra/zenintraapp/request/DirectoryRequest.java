package com.fewstones.zenintra.zenintraapp.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathieuduverney on 05/09/2016.
 */

public class DirectoryRequest extends StringRequest {

    private HashMap<String,String> params;

    public DirectoryRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener){


        super(Method.GET,url,listener,errorListener);

    }


    @Override
    public Map<String,String> getParams() {
        return params;

    }


}
