package com.fewstones.zenintra.zenintraapp.request;

/**
 * Created by mathieuduverney on 24/06/2016.
 */

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;





/**
 * Created by mathieuduverney on 22/06/2016.
 */

public class ChatRequest extends StringRequest {


    private HashMap<String,String> params;




    public ChatRequest(String token_dudu,String url,Response.Listener listener, Response.ErrorListener errorListener){


        super(Method.POST, url, listener, errorListener);


        params = new HashMap<>();
        params.put("jwt",token_dudu);

    }

    public Map<String,String> getParams(){

        return params;

    }




}

