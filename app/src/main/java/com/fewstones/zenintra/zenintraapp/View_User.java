package com.fewstones.zenintra.zenintraapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by mathieuduverney on 06/09/2016.
 */

public class View_User extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_user);


        ImageLoader imageLoader= AppController.getInstance().getImageLoader();




        Intent intent=getIntent();
        String photo_icon = intent.getStringExtra("photo");
        String first_name = intent.getStringExtra("first_name");
        String last_name = intent.getStringExtra("last_name");
        String phone_number = intent.getStringExtra("phone");
        String email = intent.getStringExtra("mail");



        TextView firstName = (TextView) findViewById(R.id.first_name_view_set);
        TextView lastName = (TextView) findViewById(R.id.last_name_view_set);
        TextView phoneNumber = (TextView) findViewById(R.id.phone_number_view_set);
        TextView mail = (TextView) findViewById(R.id.mail_view_set);

        CircularNetworkImageView image = (CircularNetworkImageView) findViewById(R.id.img_directory_view) ;




        firstName.setText(first_name);
        lastName.setText(last_name);
        phoneNumber.setText(phone_number);
        mail.setText(email);

        image.setImageUrl(photo_icon,imageLoader);





    }





}
