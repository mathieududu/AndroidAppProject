package com.fewstones.zenintra.zenintraapp.GCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.fewstones.zenintra.zenintraapp.R;
import com.fewstones.zenintra.zenintraapp.UserArea;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mathieuduverney on 04/08/2016.
 */

public class GCMPushReceiverService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data){




        String notification = data.getString("notification");



        try {
            JSONObject jsonObject = new JSONObject(notification);
            String title = jsonObject.getString("title");
            String message = jsonObject.getString("message");



            sendNotification(title,message);

        }catch (JSONException e){
            e.printStackTrace();
        }




    }


    private void sendNotification(String title, String message){

        Intent intent =new Intent(this, UserArea.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = 0; //your request code

        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);

        //setup notification
        //sound
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //build notif

        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.zen)
                .setContentTitle("New message on Zenintra")
                .setContentText( title+": \""+message+"\" " )
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                ;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,noBuilder.build());
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), sound);
        r.play();


    }

}
