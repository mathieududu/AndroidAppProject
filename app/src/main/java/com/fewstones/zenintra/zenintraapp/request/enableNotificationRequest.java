package com.fewstones.zenintra.zenintraapp.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathieuduverney on 10/08/2016.
 */

public class enableNotificationRequest extends StringRequest {

    public static final String URL_Notification = "https://zenintra.net/user/mobile_endpoint";
    private HashMap<String,String> params;



    public enableNotificationRequest(String token_dudu, String endpoint, Response.Listener<String> listener, Response.ErrorListener errorListener) {


        super(Method.POST, URL_Notification, listener, errorListener);



        params = new HashMap<>();
        params.put("jwt", token_dudu);
        params.put("endpoint",endpoint);


    }

    @Override
    public Map<String,String> getParams() {
        return params;

    }
}
