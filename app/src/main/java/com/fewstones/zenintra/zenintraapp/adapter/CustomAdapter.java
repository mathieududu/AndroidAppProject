package com.fewstones.zenintra.zenintraapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.fewstones.zenintra.zenintraapp.AppController;
import com.fewstones.zenintra.zenintraapp.CircularNetworkImageView;
import com.fewstones.zenintra.zenintraapp.R;
import com.fewstones.zenintra.zenintraapp.model.RowItem;

import java.util.List;

/**
 * Created by mathieuduverney on 21/06/2016.
 */

public class CustomAdapter extends BaseAdapter {

    Context context;
    List<RowItem> rowItem;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();


    public CustomAdapter(Context context, List<RowItem> rowItem){

        this.context=context;
        this.rowItem=rowItem;

    }

    @Override
    public  int getCount(){

        return rowItem.size();
    }

    @Override
    public Object getItem(int position) {

        return rowItem.get(position);
    }

    @Override
    public long getItemId(int position) {

        return rowItem.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_chat_item, null);
        }
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        CircularNetworkImageView imgIcon = (CircularNetworkImageView) convertView.findViewById(R.id.icon);
        TextView txtFirstLine = (TextView) convertView.findViewById(R.id.firstLine);
        TextView txtSecondLine = (TextView) convertView.findViewById(R.id.secondLine);

        RowItem row_pos = rowItem.get(position);




        // setting the image resource and title



        imgIcon.setImageUrl(row_pos.getIcon(), imageLoader);
        txtFirstLine.setText(row_pos.getFirstLine());
        txtSecondLine.setText(row_pos.getSecondLine());

        return convertView;

    }

}
