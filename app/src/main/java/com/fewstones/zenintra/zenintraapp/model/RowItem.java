package com.fewstones.zenintra.zenintraapp.model;

/**
 * Created by mathieuduverney on 21/06/2016.
 */

public class RowItem {

    private String firstLine;
    private String secondLine;
    private String icon;

    public RowItem (String firstLine, String secondLine, String icon){

        this.firstLine=firstLine;
        this.secondLine=secondLine;
        this.icon=icon;
    }

    public String getFirstLine(){

        return firstLine;
    }


    public void setFirstLine(String firstLine){

        this.firstLine=firstLine;
    }

    public String getSecondLine(){

        return secondLine;
    }


    public void setSecondLine(String secondLine){

        this.secondLine=secondLine;
    }


    public String getIcon(){

        return icon;
    }

    public void setIcon(String icon){

        this.icon=icon;
    }



}
