package com.fewstones.zenintra.zenintraapp.model;

/**
 * Created by mathieuduverney on 27/06/2016.
 */

public class MessageItem {


        private long id;
        private boolean isMe;
        private String message;
        private String firstName;

        public  MessageItem(boolean isMe, String message, String firstName){

            this.isMe = isMe;
            this.message=message;
            this.firstName=firstName;



         }

        public long getId() {
            return id;
        }
        public void setId(long id) {
            this.id = id;
        }
        public boolean getIsme() {
            return isMe;
        }
        public void setMe(boolean isMe) {
            this.isMe = isMe;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }


        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

}
