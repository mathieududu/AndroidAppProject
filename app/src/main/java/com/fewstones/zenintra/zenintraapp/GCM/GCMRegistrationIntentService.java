package com.fewstones.zenintra.zenintraapp.GCM;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.fewstones.zenintra.zenintraapp.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

/**
 * Created by mathieuduverney on 04/08/2016.
 */

public class GCMRegistrationIntentService extends IntentService {



    public static  final String REGISTRATION_SUCCESS ="Registration success";
    public static  final String REGISTRATION_ERROR ="Registration error";


    public  GCMRegistrationIntentService(){
        super("");
    }
    @Override

    protected void onHandleIntent(Intent intent) {



        registerGCM();



    }

    private void registerGCM(){
        Intent registrationComplete = null;
        String token =null;

        try {

            this.sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
            this.sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));

            InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE,null);


            // notify to UI that registration is successful

            registrationComplete = new Intent(REGISTRATION_SUCCESS);
            registrationComplete.putExtra("token",token);


        }
        catch (Exception e){


            e.printStackTrace();
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);


    }

}
