package com.fewstones.zenintra.zenintraapp.GCM;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by mathieuduverney on 04/08/2016.
 */

public class GCMTokenRefreshListenerService extends InstanceIDListenerService {


    @Override
    public void onTokenRefresh(){

        Intent intent = new Intent(this, GCMRegistrationIntentService.class);
        startService(intent);

    }


}
