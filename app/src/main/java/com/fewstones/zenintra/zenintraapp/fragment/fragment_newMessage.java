package com.fewstones.zenintra.zenintraapp.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fewstones.zenintra.zenintraapp.ChatActivity;
import com.fewstones.zenintra.zenintraapp.model.NameNewMessageItem;
import com.fewstones.zenintra.zenintraapp.adapter.NewGroupAdapter;
import com.fewstones.zenintra.zenintraapp.request.NewGroupRequest;
import com.fewstones.zenintra.zenintraapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.support.v4.app.ListFragment;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;


/**
 * Created by mathieuduverney on 07/07/2016.
 */

public class fragment_newMessage extends ListFragment implements AdapterView.OnItemClickListener{

    Context ctx;

    String token_dudu = "No token";
    int company_id;

    // First array list with my id
    ArrayList<String> chatName1 = new ArrayList<>();
    ArrayList<String> menuIcons1=new ArrayList<>();
    ArrayList<Integer> usersID1 = new ArrayList<>();


    // Second without my id
    ArrayList<String> chatName2 = new ArrayList<>();
    ArrayList<String> menuIcons2=new ArrayList<>();
    ArrayList<Integer> usersID2 = new ArrayList<>();

    //Users selectionnés
    ArrayList<Integer> usersIdChecked = new ArrayList<>();


    NewGroupAdapter newGroupAdapter;
    ArrayList<NameNewMessageItem> nameNewMessageItems = new ArrayList<>();


    // My id from NeMessage activity
    int my_id;

    EditText editText;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {

        ctx = getActivity();
        View rootview2 = inflater.inflate(R.layout.activity_fragment_newmessage, container, false);

        editText = (EditText) rootview2.findViewById(R.id.nameOfGroup);

        setHasOptionsMenu(true);

        return rootview2;

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {


        super.onActivityCreated(savedInstanceState);


        ////////////////////////////////// GET MY TOKEN ////////////////////////////////////////////////

        SharedPreferences getSharedPreferences= ctx.getSharedPreferences("MyPref",0);
        token_dudu = getSharedPreferences.getString("sharedToken", "No Token");
        my_id = getSharedPreferences.getInt("user_id", 0);
        company_id = getSharedPreferences.getInt("company_id", 0);

        ////////////////////////////////////////////////////////////////////////////////////////////////








        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {


                    JSONArray jsonArray = new JSONArray(response);

                    int number = jsonArray.length();   // length of the array



                    for (int i = 0; i < number; i++) {



                        JSONObject jsonObject = jsonArray.getJSONObject(i);


                        // --------------- First : fill in first arrayLists ---------------------



                            usersID1.add(jsonObject.getInt("id"));

                            String first_name = jsonObject.getString("first_name");
                            String last_name = jsonObject.getString("last_name");

                            chatName1.add(first_name + " " + last_name);
                            String photo = jsonObject.getString("photo");


                            if (photo.equals("null")) {
                                menuIcons1.add("https://zenintra.net/img/defaultIcon.png");
                            } else {
                                menuIcons1.add(jsonObject.getString("photo"));
                            }


                        }


                    // -------------------- Fill in second ArrayLists ----------------------


                    for (int cpt=0; cpt<number;cpt++){

                        if(usersID1.get(cpt)!=my_id){
                            usersID2.add(usersID1.get(cpt));
                            chatName2.add(chatName1.get(cpt));
                            menuIcons2.add(menuIcons1.get(cpt));

                        }
                    }


                    // --------------------- Create item with second array lists ------------


                    for(int k=0; k<number-1;k++){   // number - 1(myid)

                        NameNewMessageItem item = new NameNewMessageItem(chatName2.get(k), menuIcons2.get(k), 0);
                        nameNewMessageItems.add(item);
                    }


                    newGroupAdapter = new NewGroupAdapter(getActivity(), nameNewMessageItems);
                    setListAdapter(newGroupAdapter);



                    // initialize userIdChecked with 0

                    int i = usersID2.size() ;



                    for (int j=0; j<i ;j++){

                        usersIdChecked.add(0);
                    }




                } catch (JSONException error) {
                    error.printStackTrace();
                }
            }
        };

                Response.ErrorListener errorListener = new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                };



        NewGroupRequest newGroupRequest = new NewGroupRequest("https://zenintra.net/user/getUsersInCompany/"+company_id+"?jwt="+token_dudu,listener,errorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(ctx);
        requestQueue.add(newGroupRequest);

        getListView().setOnItemClickListener(this);



        }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (view != null) {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);

            switch (nameNewMessageItems.get(position).getValue()) {


                case 0:   //if the checkbox is not checked


                    //Check th checkbox
                    checkBox.setChecked(true);
                    nameNewMessageItems.get(position).setValue(1);

                    // set userId checked
                    usersIdChecked.set(position,usersID2.get(position));


                    //set background in grey when it is checked
                    view.setBackgroundColor(Color.GRAY);


                    break;


                case 1:    //If the checkbox is checked

                    //check the checkbox
                    checkBox.setChecked(false);
                    nameNewMessageItems.get(position).setValue(0);

                    view.setBackgroundColor(0xFFFFFF);

                    // remove the userId
                    usersIdChecked.set(position, 0);

                    break;

            }

        }


    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


            menu.clear();
            inflater.inflate(R.menu.menu_next, menu);




        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){

        switch (item.getItemId()) {

            case R.id.next:

                String NameOfGroupe = editText.getText().toString();
                ArrayList<Integer> UserCheckedWithout0 = new ArrayList<>();

                for(int i=0; i<usersIdChecked.size();i++){

                    if(usersIdChecked.get(i)!=0){
                        UserCheckedWithout0.add(usersIdChecked.get(i));
                    }
                }



                if(NameOfGroupe.equals("")){

                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx,R.style.MyAlertDialogStyle);
                    builder
                            .setMessage(R.string.EnterChatName)
                            .setNegativeButton(R.string.Retry,null)
                            .create()
                            .show();
                }

                else if(UserCheckedWithout0.size()==1 || UserCheckedWithout0.size()==0 ){

                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx,R.style.MyAlertDialogStyle);
                    builder
                            .setMessage("You have to check at least 2 people")
                            .setNegativeButton(R.string.Retry,null)
                            .create()
                            .show();

                }

                else if(!NameOfGroupe.equals("")) {


                    UserCheckedWithout0.add(32); //add my id

                    Intent intent = new Intent(ctx, ChatActivity.class);


                    intent.putExtra("id", "null"); //id null (because empty chat)
                    intent.putExtra("NameOfGroup", NameOfGroupe);  //name group
                    intent.putIntegerArrayListExtra("usersChecked", UserCheckedWithout0); // send ArrayList





                    startActivity(intent); //start intent
                }



                return true;

            default:
                super.onOptionsItemSelected(item);

            // other cases


        }

        return super.onOptionsItemSelected(item);
    };


}







