package com.fewstones.zenintra.zenintraapp.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;


import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fewstones.zenintra.zenintraapp.ChatActivity;
import com.fewstones.zenintra.zenintraapp.adapter.CustomAdapter;
import com.fewstones.zenintra.zenintraapp.request.ListChatRequest;
import com.fewstones.zenintra.zenintraapp.LoginActivity;
import com.fewstones.zenintra.zenintraapp.NewMessage;
import com.fewstones.zenintra.zenintraapp.R;
import com.fewstones.zenintra.zenintraapp.model.RowItem;
import com.fewstones.zenintra.zenintraapp.UserArea;
import com.fewstones.zenintra.zenintraapp.request.disableNotificationRequest;
import com.fewstones.zenintra.zenintraapp.request.enableNotificationRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;


import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;



public class fragment_listChat extends ListFragment implements AdapterView.OnItemClickListener{


    ArrayList<String> chat_type = new ArrayList<>();
    ArrayList<String> menuIcons=new ArrayList<>();
    ArrayList<String> chatName = new ArrayList<>();
    ArrayList<String> chat_id = new ArrayList<>();

    Context ctx;
    CustomAdapter adapter;
    private List<RowItem> rowItems;

    private ArrayList<Integer> user1_chat= new ArrayList<>();
    private ArrayList<Integer> user2_chat= new ArrayList<>();

    protected  int MYID = -1; //use shared preference
    private int group1Id = 1;
    int homeId = Menu.FIRST;

    private SwipeRefreshLayout swipeRefreshLayout;
    private Handler handler = new Handler();

    private String token_dudu ="No token";
    private String enablenotif = "";

    private boolean notif = false;

    private Menu menu;







  @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {

      ctx = getActivity();


      View rootview = inflater.inflate(R.layout.activity_fragment_list_chat, container, false);




      setHasOptionsMenu(true);


      return rootview;

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState){



        super.onActivityCreated(savedInstanceState);









        final SharedPreferences sharedPreferences = ctx.getSharedPreferences("MyPref",0);
        token_dudu = sharedPreferences.getString("sharedToken", "No token");








        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    rowItems = new ArrayList<RowItem>();
                    JSONArray jsonArray = new JSONArray(response);

                    int number = jsonArray.length();   // length of the array

                    for(int i=0; i<number;i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        JSONArray jsonArrayUser = jsonObject.getJSONArray("users");


                        chatName.add(jsonObject.getString("chat_name"));   //Add chat_name in the List chatName
                        menuIcons.add(jsonObject.getString("photo"));       //Add the photo in the List MenuIcons
                        chat_type.add(jsonObject.getString("chat_type"));   //Add chat_typein the List chat_type
                        chat_id.add(jsonObject.getString("id")); //add id of the




                        JSONObject id1 = jsonArrayUser.getJSONObject(0);
                        user1_chat.add(id1.getInt("id"));
                        JSONObject id2 = jsonArrayUser.getJSONObject(1);
                        user2_chat.add(id2.getInt("id"));




                        RowItem items = new RowItem(chatName.get(i), chat_type.get(i), menuIcons.get(i));   //Create new row
                        rowItems.add(items);   //Add the row
                    }


                    adapter = new CustomAdapter(getActivity(), rowItems);
                    setListAdapter(adapter);


                } catch (JSONException e){

                    e.printStackTrace();
                }
            }
        };

        //Error

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };



        ListChatRequest listChatRequest = new ListChatRequest(token_dudu,listener,errorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(ctx);
        requestQueue.add(listChatRequest);


        getListView().setOnItemClickListener(this);




    }




    // open a chat


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

       // Toast.makeText(getActivity(), chat_id.get(position), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(getActivity().getBaseContext(), ChatActivity.class);
        intent.putExtra("id", chat_id.get(position));  //send id to ChatActivity

        ArrayList<Integer> usersClicked = new ArrayList<>();
        usersClicked.add(user1_chat.get(position));
        usersClicked.add(user2_chat.get(position));

        intent.putIntegerArrayListExtra("usersChecked", usersClicked);



        //number of loop
        intent.putExtra("loop",2);

        getActivity().startActivity(intent);


    }





    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {



        SharedPreferences sharedPreferences = ctx.getSharedPreferences("MyPref",0);
        enablenotif=sharedPreferences.getString("stringNotif","Enable notifications");

        inflater.inflate(R.menu.menu_main, menu);




        menu.add(Menu.NONE, R.id.menu_action1, Menu.NONE, "Logout");
        menu.add(Menu.NONE, R.id.menu_action2, Menu.NONE, enablenotif);

        super.onCreateOptionsMenu(menu, inflater);

        this.menu =menu;



    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){



        switch (item.getItemId()) {

            case R.id.new_message:


                Intent intent = new Intent(ctx, NewMessage.class);

                startActivity(intent);

                return true;

            case R.id.menu_action1:

                final SharedPreferences preferencesToken = ctx.getSharedPreferences("MyPref",0);


                SharedPreferences.Editor editor = preferencesToken.edit();
                editor.remove("sharedToken");
                editor.remove("user_id");
                editor.remove("company_id");
                editor.apply();


                Intent intent1 = new Intent(ctx,LoginActivity.class);
                startActivity(intent1);

                return true;


            case R.id.menu_action2:

                UserArea userArea = (UserArea) getActivity();
                String tokenGCM = userArea.getToken();



                final SharedPreferences sharedPreferences = ctx.getSharedPreferences("MyPref",0);
                notif=sharedPreferences.getBoolean("notif",false);



               SharedPreferences.Editor editor1 = sharedPreferences.edit();



                Response.Listener notifListener = new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {

                        try {
                            String result = response.toString();




                        } catch (Exception e){
                                e.printStackTrace();
                        }
                    }
                };

                Response.ErrorListener errorListener = new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                };




                if(!notif) {



                    enableNotificationRequest enableNotificationRequest = new enableNotificationRequest(token_dudu, "https://android.googleapis.com/gcm/send/" + tokenGCM, notifListener, errorListener);
                    RequestQueue requestQueue1 = Volley.newRequestQueue(ctx);
                    requestQueue1.add(enableNotificationRequest);


                    editor1.putBoolean("notif",true);
                    editor1.putString("stringNotif","Disable Notifications");
                    editor1.commit();
                    enablenotif = sharedPreferences.getString("stringNotif","Error notification");
                    menu.findItem(R.id.menu_action2).setTitle(enablenotif);



                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx,R.style.MyAlertDialogStyle);
                    builder
                            .setMessage("Notifications enabled")
                            .setNegativeButton("Got it",null)
                            .create()
                            .show();


                }

                else if(notif){




                    disableNotificationRequest disableNotificationRequest = new disableNotificationRequest(token_dudu, "https://android.googleapis.com/gcm/send/"+tokenGCM,notifListener,errorListener);
                    RequestQueue requestQueue2 = Volley.newRequestQueue(ctx);
                    requestQueue2.add(disableNotificationRequest);

                    editor1.putBoolean("notif",false);
                    editor1.putString("stringNotif","Enable Notifications");
                    editor1.commit();
                    enablenotif = sharedPreferences.getString("stringNotif","Error notification");


                    menu.findItem(R.id.menu_action2).setTitle(enablenotif);

                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                    builder
                            .setMessage("\nNotifications disabled\n")
                            .setNegativeButton("Got it",null)
                            .create()
                            .show();




                }


                return true;




            default:
                super.onOptionsItemSelected(item);

                // other cases


        }

        return super.onOptionsItemSelected(item);

    };













}
