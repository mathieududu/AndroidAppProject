package com.fewstones.zenintra.zenintraapp.model;

/**
 * Created by mathieuduverney on 07/07/2016.
 */

public class NameNewMessageItem {

    private String firstLine;
    private String icon;
    private int value;

    public NameNewMessageItem(String firstLine, String icon, int value ) {

        super();
        this.firstLine = firstLine;
        this.icon = icon;
        this.value=value;

    }

    public String getFirstLine() {

        return firstLine;
    }


    public void setFirstLine(String firstLine) {

        this.firstLine = firstLine;
    }


    public String getIcon(){

        return icon;
    }

    public void setIcon(String icon){

        this.icon=icon;
    }

    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }

}
