package com.fewstones.zenintra.zenintraapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.fewstones.zenintra.zenintraapp.AppController;
import com.fewstones.zenintra.zenintraapp.CircularNetworkImageView;
import com.fewstones.zenintra.zenintraapp.model.NameNewMessageItem;
import com.fewstones.zenintra.zenintraapp.R;

import java.util.List;

/**
 * Created by mathieuduverney on 07/07/2016.
 */

public class NewGroupAdapter extends BaseAdapter {


    Context context;
    List<NameNewMessageItem> nameNewMessageItems;
    ImageLoader imageLoader= AppController.getInstance().getImageLoader();

   public  NewGroupAdapter (Context context, List<NameNewMessageItem> nameNewMessageItems){


        this.context=context;
        this.nameNewMessageItems=nameNewMessageItems;
    }


    @Override
    public int getCount() {
        return nameNewMessageItems.size();
    }

    @Override
    public Object getItem(int position) {
        return nameNewMessageItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return nameNewMessageItems.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.newmessage_item, null);
        }
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        CircularNetworkImageView imgIcon = (CircularNetworkImageView) convertView.findViewById(R.id.icon);
        TextView txtFirstLine = (TextView) convertView.findViewById(R.id.nameNewMessage);
        CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);


        NameNewMessageItem Name_pos = nameNewMessageItems.get(position);

        imgIcon.setImageUrl(Name_pos.getIcon(), imageLoader);
        txtFirstLine.setText(Name_pos.getFirstLine());

        if(Name_pos.getValue() == 1)
            checkBox.setChecked(true);
        else
            checkBox.setChecked(false);




        return convertView;
    }

}
