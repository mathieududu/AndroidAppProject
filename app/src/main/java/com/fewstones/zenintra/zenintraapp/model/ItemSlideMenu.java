package com.fewstones.zenintra.zenintraapp.model;

/**
 * Created by mathieuduverney on 01/09/2016.
 */

public class ItemSlideMenu {

    private int imgID;
    private String title;


    public ItemSlideMenu(int imgID, String title){

        this.imgID=imgID;
        this.title=title;

    }


    public int getImgID(){

        return imgID;

    }

    public String getTitle(){

        return title;

    }

    public void setImgID(int imgID) {
        this.imgID = imgID;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
