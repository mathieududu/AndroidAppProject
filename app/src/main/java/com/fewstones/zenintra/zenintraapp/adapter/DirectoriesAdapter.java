package com.fewstones.zenintra.zenintraapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.fewstones.zenintra.zenintraapp.AppController;
import com.fewstones.zenintra.zenintraapp.CircularNetworkImageView;
import com.fewstones.zenintra.zenintraapp.model.DirectoryItem;
import com.fewstones.zenintra.zenintraapp.R;
import com.fewstones.zenintra.zenintraapp.fragment.fragment_directories;


import java.util.List;

/**
 * Created by mathieuduverney on 02/09/2016.
 */

public class DirectoriesAdapter extends BaseAdapter {

    Context ctx;
    List<DirectoryItem> directoryItems;
    ImageLoader imageLoader= AppController.getInstance().getImageLoader();
    ImageView test;
    fragment_directories fragment;

    public DirectoriesAdapter(Context ctx, List<DirectoryItem> directoryItems, fragment_directories fragment){

        this.ctx=ctx;
        this.directoryItems=directoryItems;
        this.fragment=fragment;

    }

    @Override
    public int getCount() {
        return directoryItems.size();
    }

    @Override
    public Object getItem(int position) {
        return directoryItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return directoryItems.indexOf(getItem(position));
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.directory_item, null);
        }
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        CircularNetworkImageView imgIcon_directory = (CircularNetworkImageView) convertView.findViewById(R.id.img_directory);
        final TextView name = (TextView) convertView.findViewById(R.id.name_directory);
        final ImageView call = (ImageView) convertView.findViewById(R.id.icon_call);
        ImageView mail = (ImageView) convertView.findViewById(R.id.icon_mail);
        ImageView info = (ImageView) convertView.findViewById(R.id.icon_view);

        DirectoryItem directoryItem_position = directoryItems.get(position);
        imgIcon_directory.setImageUrl(directoryItem_position.getImg_directory(),imageLoader);
        name.setText(directoryItem_position.getName_directory());

        call.setTag(position);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer pos = (Integer)v.getTag();
                fragment.call_number(pos);
            }
        });

        mail.setTag(position);
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Integer pos = (Integer)v.getTag();
                fragment.send_mail(pos);
            }
        });

        info.setTag(position);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer pos = (Integer)v.getTag();
                fragment.open_info(pos);
            }
        });

        return convertView;
    }

}
