package com.fewstones.zenintra.zenintraapp.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathieuduverney on 07/06/2016.
 */
public class LoginRequest extends StringRequest{



    private static final String LOGIN_REQUEST_URL = "https://zenintra.net/mobile_login";
    private HashMap <String, String> params;




    public LoginRequest (String username, String password, int platform, Response.Listener<String> listener, Response.ErrorListener errorListener){


        super(Method.POST,LOGIN_REQUEST_URL,listener,errorListener);
        params = new HashMap<>();
        params.put("username", username);
        params.put("password",password);
        params.put("platform", platform+"");


    }



    @Override
    public Map<String,String> getParams() {
        return params;

    }





}







