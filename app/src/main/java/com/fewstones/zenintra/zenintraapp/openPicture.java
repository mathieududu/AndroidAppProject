package com.fewstones.zenintra.zenintraapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fewstones.zenintra.zenintraapp.request.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathieuduverney on 23/08/2016.
 */

public class openPicture extends AppCompatActivity {

    private ImageView imgView;
    private ImageButton send;
    private ImageButton cancel;
    private String img;
    private Bitmap bitmap;
    private static final String URL_UPLOAD_PICTURE = "https://zenintra.net/chat/upload_file";
    ArrayList<Integer> users_id = new ArrayList<>();
    String token_dudu;
    String chat_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_open_picture);

        imgView = (ImageView) findViewById(R.id.surfaceView);
        send = (ImageButton) findViewById(R.id.sendPicture);
        cancel = (ImageButton) findViewById(R.id.cancelPicture);


        Intent intent = getIntent();

        img = intent.getStringExtra("image");
        users_id = intent.getIntegerArrayListExtra("users_id");
        chat_id = intent.getStringExtra("chat_id");






        /////// prevent ERROR : OutOfMemory ///////






       DisplayBitmap(imgView,img);


        /////////////////////////////////////////////





        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();

            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                sendFile();


            }
        });


    }




    @Override
    public void onSaveInstanceState(Bundle toSave) {
        super.onSaveInstanceState(toSave);
//        toSave.putParcelable("bitmap", BitmapFactory.decodeFile(img));
        toSave.putString("bitmap",img);
    }

    private void sendFile() {
        // loading or check internet connection or something...
        // ... then
        String URL_UPLOAD_PICTURE = "https://zenintra.net/chat/upload_file";
        final SharedPreferences getToken = this.getSharedPreferences("MyPref", 0);

        token_dudu=getToken.getString("sharedToken","No token");

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,URL_UPLOAD_PICTURE, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {



            }
        })



        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                try {

                    // ------------- in users array -----------


                    JSONArray jsonArray_users = new JSONArray();


                    JSONObject id1 = new JSONObject();      //Create id[]
                    id1.put("id",users_id.get(0));

                    JSONObject id2 = new JSONObject();
                    id2.put("id",users_id.get(1));


                    jsonArray_users.put(id1);  //insert id in the user array
                    jsonArray_users.put(id2);



                    //// --------- in active_chat object ----------

                    JSONObject jsonObject_one = new JSONObject();

                    jsonObject_one.put("id",chat_id);
                    jsonObject_one.put("users",jsonArray_users);
                    jsonObject_one.put("chat_intent","group");
                    jsonObject_one.put("chat_type","group");



                    // ------------- Root object ---------


                    params.put("active_chat",jsonObject_one.toString(13)); //insert jsonObject_One in active_chat
                    params.put("text","");
                    params.put("jwt",token_dudu);

                }

                catch (JSONException e) {
                    e.printStackTrace();

                }
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("file", new DataPart(img, AppHelper.getFileDataFromDrawable(getBaseContext(), imgView.getDrawable()), "image/jpg"));


                return params;
            }
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("id",chat_id);
        intent.putIntegerArrayListExtra("usersChecked",users_id);
        startActivity(intent);


    }


    public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }



    private void DisplayBitmap(ImageView imageView, String img){


        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            bitmap = BitmapFactory.decodeFile(img, options);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();

            System.gc();

            try {
                bitmap = BitmapFactory.decodeFile(img);
            } catch (OutOfMemoryError e2) {
                e2.printStackTrace();

                // handle gracefully.
            }
        }

        imageView.setImageBitmap(bitmap);


    }

    private boolean shouldAskPermission(){

        return(Build.VERSION.SDK_INT> Build.VERSION_CODES.LOLLIPOP_MR1);

    }
}
