package com.fewstones.zenintra.zenintraapp.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fewstones.zenintra.zenintraapp.ChatActivity;
import com.fewstones.zenintra.zenintraapp.request.ChatRequest;
import com.fewstones.zenintra.zenintraapp.adapter.MessageAdapter;
import com.fewstones.zenintra.zenintraapp.model.MessageItem;
import com.fewstones.zenintra.zenintraapp.R;
import com.fewstones.zenintra.zenintraapp.request.SendMessageRequest;
import com.fewstones.zenintra.zenintraapp.request.SendMessageToEmptyChatRequest;
import com.fewstones.zenintra.zenintraapp.UserArea;
import com.fewstones.zenintra.zenintraapp.openPicture;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.ArrayList;

import android.os.Handler;

//import com.github.nkzawa.socketio.client.Socket;
//import com.github.nkzawa.emitter.Emitter;
//import com.github.nkzawa.socketio.client.IO;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static android.app.Activity.RESULT_OK;


/**
 * Created by mathieuduverney on 24/06/2016.
 */

public class fragment_chatMessage extends ListFragment {


    // String from ChatActivity
    String id = "HOLA";
    Context ctx;



    private String token_dudu="";
    private int company_id;

    ArrayList<String> chatName = new ArrayList<>(); // get chat name
    ArrayList<String> user_id = new ArrayList<>();   // get user id
    ArrayList<String> text_content = new ArrayList<>();  // text
    ArrayList<String> first_name = new ArrayList<>();   // messages' owner




    private EditText messageET;
    private ListView messagesContainer;
    private RelativeLayout containerChat;
    private Button sendBtn;
    private MessageAdapter adapter;
    private ImageButton uploadFile;
    private ImageView imgView;
    private Button sendButton;
    private Button cancelButton;
    private TextView chat_name;
    private LinearLayout Ll;
    private LinearLayout Lp;



    public Handler mHandler;
    public View ftView;
    public boolean isLoading = false;


    private ArrayList<MessageItem> messagesItems = new ArrayList<>();


    private  TextView textViewChatName;

    private String TRUE_USER ="TRUE USER";
    private String MESSAGE_SEND="MESSAGE SEND";
    private String MYFIRSTNAME="Me";
    private int CHAT_ID=0;
    private  String CHAT_TYPE = "CHAT TYPE";
    private  String MEDIA_CONTENT;
    private  String MEDIA_CONTENT_SPLIT;
    private String MEDIA_CONTENT_FINAL;
    private String MEDIA_CONTENT_DECODE;

    private boolean isMe = true;


    ArrayList<Integer> users_id = new ArrayList<>();
    String nameNewChat = "";


    String chat_server = "https://chat.zenintra.net";


    IO.Options options = new IO.Options();

    String imgDecodableString;



    //Update listview
    private int nineMsgFirst;
    private int cptForUpdate=1;
    private boolean finish_loading=false;
    private int NUMBER_MESSAGE_DISPLAY=30;






    private Socket mSocket;


    // Socket io response

    private Emitter.Listener onNewMessage = new Emitter.Listener() {


        @Override
        public void call(final Object... args) {




            if(getActivity() == null)
                return;


            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {






                    int chat_id;
                    int userID;

                    boolean isME = false;

                    String firstName;
                    String message;
                    String media_content_socket="null";
                    String media_content_split;
                    String media_content_final;




                    ChatActivity chatActivity = (ChatActivity) getActivity();
                    String chatID = chatActivity.getMyData();

                    SharedPreferences sharedPreferences = ctx.getSharedPreferences("MyPref", 0);
                    int MYID = sharedPreferences.getInt("user_id", 0);











                    try {



                        JSONObject data = new JSONObject((String) args[0]);
                        JSONObject chat_line = data.getJSONObject("chat_line");

                        Log.d("Args",args[0]+"");




                      try{
                          media_content_socket = chat_line.getString("media_content");


                      }catch (Exception e) {


                          e.printStackTrace();

                      }

                        if(media_content_socket.equals("null")){

                            message = chat_line.getString("text_content");     //get the text content

                        } else {

                            media_content_split = media_content_socket.substring(media_content_socket.lastIndexOf("/")+ 1);

                            media_content_final = "<a href=\""+media_content_socket+"\"> "+media_content_split+" </a>";


                            message = chat_line.getString("text_content")+"\n"+media_content_final;



                        }

                        chat_id = chat_line.getInt("chat_id");

                        JSONObject user = chat_line.getJSONObject("user");
                        firstName = user.getString("first_name");

                        userID = user.getInt("id");



                        if (userID==MYID){
                            firstName = "Me";
                            isME = true;
                        }













                    } catch (JSONException e) {

                        e.printStackTrace();


                        return;
                    }



                    // add the message to view if the chat id is right

                    if(chatID.equals(chat_id+"")) {
                        MessageItem messageItem = new MessageItem(isME, message, firstName);
                        messagesItems.add(messageItem); //add a bubble

                        adapter = new MessageAdapter(getActivity(), messagesItems);
                        setListAdapter(adapter);

                        messagesContainer.setSelection(messagesContainer.getCount() -1);

                    }

                }
            });
        }
    };





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {


        ctx = getActivity();




        try {


            final  SharedPreferences getToken = ctx.getSharedPreferences("MyPref", 0);

            token_dudu=getToken.getString("sharedToken","No token");
            company_id=getToken.getInt("company_id",-1);

            options.query="token="+token_dudu+"&company_id="+company_id ;

            mSocket = IO.socket(chat_server,options);



        } catch (URISyntaxException e) {


        }



        View rootview = inflater.inflate(R.layout.activity_fragment_chat_message,null,false);


        messageET = (EditText) rootview.findViewById(R.id.messageEdit);
        sendBtn = (Button) rootview.findViewById(R.id.chatSendButton);
        containerChat = (RelativeLayout) rootview.findViewById(R.id.container);
        textViewChatName = (TextView) rootview.findViewById(R.id.NameOfRecepient);
        messagesContainer = (ListView) rootview.findViewById(android.R.id.list);
        uploadFile = (ImageButton) rootview.findViewById(R.id.uploadFile);
        imgView = (ImageView) rootview.findViewById(R.id.imageView);

        chat_name = (TextView) rootview.findViewById(R.id.NameOfRecepient);

        LayoutInflater li = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ftView = li.inflate(R.layout.footer_view,null);
        mHandler= new MyHandler();

        Ll = (LinearLayout) rootview.findViewById(R.id.includeFtview);


        // Nous récupérons notre surface pour le preview
        //sendBtn = (Button) rootview.findViewById(R.id.sendPicture);
        //cancelButton= (Button) rootview.findViewById(R.id.cancelPicture);






        // ------------------- disable button SEND if text's empty -----------------------------------


        String s1 = messageET.getText().toString();

        if(s1.equals("")){
            sendBtn.setEnabled(false);
           sendBtn.getBackground().setAlpha(128);
        } else {
            sendBtn.setEnabled(true);
            sendBtn.getBackground().setAlpha(255);

        }


        messageET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String s1 = messageET.getText().toString();

                if(s1.equals("")){
                    sendBtn.setEnabled(false);
                    sendBtn.getBackground().setAlpha(128);
                } else {
                    sendBtn.setEnabled(true);
                    sendBtn.getBackground().setAlpha(255);

                }

            }
        });


        // ----------------------------------------------------------------------------------------






        mSocket.connect();
        mSocket.on("chat.message", onNewMessage);





        return rootview;

    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {


        super.onActivityCreated(savedInstanceState);



        //////////////////////////////////////////////////////////////////////////////////////////

        final  SharedPreferences getToken = ctx.getSharedPreferences("MyPref", 0);

        token_dudu=getToken.getString("sharedToken","No token");

        company_id=getToken.getInt("company_id",-1);




        //////////////////////////////////////////////////////////////////////////////////////////


        //Get the id from ChatActivity
        ChatActivity activity = (ChatActivity) getActivity();
        id = activity.getMyData();   //id from fragment_newMessage == null if it's a new chat
        users_id = activity.getUsers();
        nameNewChat =activity.getChat_name();





        //////////////////////////////////////////////////////////////////////////////////////////




        // --------------------------------------------------------------------------

        // Response listener

        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{


                    if(id!=null) {


                        JSONObject jsonObject = new JSONObject(response);


                        // -------------------------- Get ChatName ------------------------------

                        chatName.add(jsonObject.getString("chat_name"));   //Add chat_name in the List chatName


                        chat_name.setText(chatName.get(0));   // Display chatName at the top

                        // ------------------------ Get chat_type -------------------------------

                        CHAT_TYPE = jsonObject.getString("chat_type");




                        // ------------------- Access to Chat_Line Array   -----------------------


                        JSONArray jsonArrayChatLine = jsonObject.getJSONArray("chat_lines");


                        // --------------------- Access to pivot ---------------------------------

                        JSONObject pivot = jsonObject.getJSONObject("pivot");

                        TRUE_USER = pivot.getString("user_id");    //get the user_id of the pivot
                        CHAT_ID = pivot.getInt("chat_id");       //get chat_id


                        // ---------------  Get user_id & text_content & first Name --------------




                        int chatLineArgs = jsonArrayChatLine.length();  // for the loop


                        nineMsgFirst = chatLineArgs -30;


                        if (nineMsgFirst<0){
                            nineMsgFirst=0;
                        }



                                    int k=0;
                                    for (int i = nineMsgFirst; i < chatLineArgs; i++) {





                                        JSONObject jsonObjectCL = jsonArrayChatLine.getJSONObject(i);

                                        user_id.add(jsonObjectCL.getString("user_id"));  //get user_id of chat_line
                                        MEDIA_CONTENT = jsonObjectCL.getString("media_content");
                                        if(MEDIA_CONTENT.equals("null")){

                                            text_content.add(jsonObjectCL.getString("text_content"));     //get the text content

                                        } else {
                                            MEDIA_CONTENT_SPLIT = MEDIA_CONTENT.substring(MEDIA_CONTENT.lastIndexOf("/")+ 1);

                                            MEDIA_CONTENT_FINAL = "<a href=\""+MEDIA_CONTENT+"\"> "+MEDIA_CONTENT_SPLIT+" </a>";


                                            try {

                                                MEDIA_CONTENT_DECODE = URLDecoder.decode(MEDIA_CONTENT_FINAL, "UTF-8");

                                            }catch (UnsupportedEncodingException e){

                                             e.printStackTrace();

                                            }




                                            text_content.add(MEDIA_CONTENT_DECODE);
                                        }






                                            JSONObject jsonObjectUser = jsonObjectCL.getJSONObject("user"); //access to the user object


                                            if (!user_id.get(k).equals(TRUE_USER)) {

                                                first_name.add(jsonObjectUser.getString("first_name"));      //get the first name of each message

                                            } else {

                                                first_name.add("Me");
                                            }


                                            // To display messages on right side


                                            if (user_id.get(k).equals(TRUE_USER)) {

                                                isMe = true;


                                            } else if (!user_id.get(k).equals(TRUE_USER)) {

                                                isMe = false;
                                            }


                                            //--------------------- DISPLAY MY FIRSTNAME ----------------------

                                            if (user_id.get(k).equals(TRUE_USER)) {

                                                MYFIRSTNAME = "Me";

                                            }


                                            MessageItem messageItem = new MessageItem(isMe, text_content.get(k), first_name.get(k));  // create bubble

                                            messagesItems.add(messageItem); //add a bubble

                                        k=k+1;


                                        }







                        // display list

                        adapter = new MessageAdapter(getActivity(), messagesItems);
                        setListAdapter(adapter);

                        cptForUpdate++;


                        messagesContainer.setSelection(messagesContainer.getCount() -1);




                    }


                }catch (JSONException e){


                }
            }
        };

        //Error

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {



                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder
                        .setMessage("Problem unknown")
                        .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(ctx, UserArea.class);
                                ctx.startActivity(intent);


                            }
                        })
                        .create()
                        .show();
            }
        };



        ///////////////////////////////////////   Upload pictures from gallery ///////////////////////////////////////



        //----------------------------------- Open menu ---------------------------------------------------



        registerForContextMenu(uploadFile);


        uploadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.showContextMenu();

            }
        });






        // ---------------------- If the chat is not empty : display old messages ------------------------


        if(!id.equals("null")) {





            ChatRequest ChatRequest = new ChatRequest(token_dudu,"https://zenintra.net/chat_messages/" + id, listener, errorListener);
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            requestQueue.add(ChatRequest);



            messagesContainer.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {



                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {


                    //Check when scroll to last item in listview,
                    if (view.getFirstVisiblePosition()==0 && messagesContainer.getCount()>=NUMBER_MESSAGE_DISPLAY && isLoading==false && finish_loading==false){


                        isLoading=true;
                        Thread thread = new ThreadGetMoreData();
                        //start thread
                        thread.start();


                    }

                }
            });




        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////  SEND A MESSAGE ///////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


       sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MESSAGE_SEND =messageET.getText().toString();


                if (TextUtils.isEmpty(MESSAGE_SEND)) {
                    return;
                }



                
                //------------------- Response SEND A MESSAGE [not empty chat] --------------------




                Response.Listener<String> sendMessage = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                };



                // -------------------- Response SEND MESSAGE [empty chat] ---------------------------



                Response.Listener<String> sendMessageEmptyChat = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }

                };



                // ----------------------------- Error in both case ----------------------------------




                Response.ErrorListener errorSendMessage = new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                };



                //-----------------------------  chat not empty --------------------------------------




                if(!id.equals("null")) {


                    SendMessageRequest sendMessageRequest = new SendMessageRequest(token_dudu,MESSAGE_SEND, CHAT_ID, users_id.get(0),users_id.get(1), sendMessage, errorSendMessage);  //(user1 and user2 useless)
                    RequestQueue sendRequest = Volley.newRequestQueue(ctx);
                    sendRequest.add(sendMessageRequest);



                    // stay at the bottom and clear editText

                    messagesContainer.setSelection(messagesContainer.getCount() -1);

                    messageET.setText("");


                }


                // --------------------------  empty chat (id = "null") --------------------------------------



                if(id.equals("null")) {


                    if(users_id.size()>2) {



                        SendMessageToEmptyChatRequest sendMessageToEmptyChatRequest = new SendMessageToEmptyChatRequest(token_dudu,MESSAGE_SEND, users_id, nameNewChat, sendMessageEmptyChat, errorSendMessage);

                        RequestQueue sendRequestChatEmpty = Volley.newRequestQueue(ctx);
                        sendRequestChatEmpty.add(sendMessageToEmptyChatRequest);
                    }
                    else {

                        SendMessageToEmptyChatRequest sendMessageToEmptyChatRequest = new SendMessageToEmptyChatRequest(token_dudu, MESSAGE_SEND, users_id, "one-to-one", sendMessageEmptyChat, errorSendMessage);

                        RequestQueue sendRequestChatEmpty = Volley.newRequestQueue(ctx);
                        sendRequestChatEmpty.add(sendMessageToEmptyChatRequest);

                    }


                    // Create new bubble

                    MessageItem messageSend = new MessageItem(true,MESSAGE_SEND,"Me");
                    messagesItems.add(messageSend);

                    adapter = new MessageAdapter(getActivity(), messagesItems);
                    setListAdapter(adapter);

                    //stay at the bottom and clear editText

                    messagesContainer.setSelection(messagesContainer.getCount() -1);
                    messageET.setText("");

                }

            }
       });


    }




    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        getActivity().getMenuInflater().inflate(R.menu.menu_upload, menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.openCamera:



               Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                startActivityForResult(intent,2);

                return true;

            case R.id.openGallery:



                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, 1);

                return true;
        }
        return super.onContextItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {


            // When an Image is picked
            if (requestCode == 1 && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = ctx.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);

                cursor.close();


                Intent gallery = new Intent(ctx, openPicture.class);
                gallery.putExtra("image", imgDecodableString);
                gallery.putIntegerArrayListExtra("users_id",users_id);
                gallery.putExtra("chat_id",id);



                startActivity(gallery);







                // Set the Image in ImageView after decoding the String

              /*  imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                imgView.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                sendBtn.setVisibility(View.VISIBLE);*/









            } else {
                Toast.makeText(ctx, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
           e.printStackTrace();
        }


        if (requestCode == 2) {






            Uri uri=data.getData();

            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            // Get the cursor
            Cursor cursor = ctx.getContentResolver().query(uri, filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);

            cursor.close();





            Intent camera = new Intent(ctx, openPicture.class);
            camera.putExtra("image", imgDecodableString);
            camera.putIntegerArrayListExtra("users_id",users_id);
            camera.putExtra("chat_id",id);



            startActivity(camera);




        }

    }



    public class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg){
            switch (msg.what){

                case 0:

                    //add Loading view during search processing


                    if(Build.VERSION.SDK_INT<19) {
                        messagesContainer.setAdapter(null);
                        Ll.setVisibility(View.VISIBLE);

                    }
                    messagesContainer.addHeaderView(ftView);
                    break;

                case 1:
                    //update data adapter and UI
                //    adapter.addListItemtoAdapter((ArrayList<MessageItem>)msg.obj);

                    //Remove loading view after update listview
                    Ll.setVisibility(View.GONE);
                    messagesContainer.removeHeaderView(ftView);

                    isLoading=false;
                    break;

                default:
                    break;


            }

        }};


    private ArrayList<MessageItem> getMoreData(){



        // add here update

        /*list.add(new MessageItem(true, "text_test1", "Name_test"));
        list.add(new MessageItem(true, "text_test2", "Name_test"));
        list.add(new MessageItem(true, "text_test1", "Name_test"));
        list.add(new MessageItem(true, "text_test2", "Name_test"));
        list.add(new MessageItem(true, "text_test1", "Name_test"));
        list.add(new MessageItem(true, "text_test2", "Name_test"));
        list.add(new MessageItem(true, "text_test1", "Name_test"));
        list.add(new MessageItem(true, "text_test2", "Name_test"));*/


        Response.Listener<String> listenerUpdate = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (!finish_loading){
                    try {


                        if (id != null) {


                            text_content.clear();
                            user_id.clear();
                            first_name.clear();
                            chatName.clear();
                            messagesItems.clear();

                            messagesContainer.setAdapter(null);


                            JSONObject jsonObject = new JSONObject(response);


                            // -------------------------- Get ChatName ------------------------------

                            chatName.add(jsonObject.getString("chat_name"));   //Add chat_name in the List chatName


                            chat_name.setText(chatName.get(0));   // Display chatName at the top

                            // ------------------------ Get chat_type -------------------------------

                            CHAT_TYPE = jsonObject.getString("chat_type");




                            // ------------------- Access to Chat_Line Array   -----------------------


                            JSONArray jsonArrayChatLine = jsonObject.getJSONArray("chat_lines");


                            // --------------------- Access to pivot ---------------------------------

                            JSONObject pivot = jsonObject.getJSONObject("pivot");

                            TRUE_USER = pivot.getString("user_id");    //get the user_id of the pivot
                            CHAT_ID = pivot.getInt("chat_id");       //get chat_id


                            // ---------------  Get user_id & text_content & first Name --------------


                            int chatLineArgs = jsonArrayChatLine.length(); // for the loop

                            if (nineMsgFirst > 0) {
                                nineMsgFirst = nineMsgFirst - NUMBER_MESSAGE_DISPLAY;
                            }

                            if (nineMsgFirst < 0) {

                                int negatif=nineMsgFirst;
                                nineMsgFirst = 0;






                                int k = 0;
                                for (int i = nineMsgFirst; i < chatLineArgs; i++) {

                                    JSONObject jsonObjectCL = jsonArrayChatLine.getJSONObject(i);

                                    user_id.add(jsonObjectCL.getString("user_id"));  //get user_id of chat_line
                                    MEDIA_CONTENT = jsonObjectCL.getString("media_content");
                                    if (MEDIA_CONTENT.equals("null")) {

                                        text_content.add(jsonObjectCL.getString("text_content"));     //get the text content

                                    } else {
                                        MEDIA_CONTENT_SPLIT = MEDIA_CONTENT.substring(MEDIA_CONTENT.lastIndexOf("/") + 1);

                                        MEDIA_CONTENT_FINAL = "<a href=\"" + MEDIA_CONTENT + "\"> " + MEDIA_CONTENT_SPLIT + " </a>";


                                        try {

                                            MEDIA_CONTENT_DECODE = URLDecoder.decode(MEDIA_CONTENT_FINAL, "UTF-8");

                                        } catch (UnsupportedEncodingException e) {

                                            e.printStackTrace();

                                        }


                                        text_content.add(MEDIA_CONTENT_DECODE);
                                    }


                                    JSONObject jsonObjectUser = jsonObjectCL.getJSONObject("user"); //access to the user object

                                    if (!user_id.get(k).equals(TRUE_USER)) {

                                        first_name.add(jsonObjectUser.getString("first_name"));      //get the first name of each message

                                    } else {

                                        first_name.add("Me");
                                    }


                                    // To display messages on right side


                                    if (user_id.get(k).equals(TRUE_USER)) {

                                        isMe = true;


                                    } else if (!user_id.get(k).equals(TRUE_USER)) {

                                        isMe = false;
                                    }


                                    //--------------------- DISPLAY MY FIRSTNAME ----------------------

                                    if (user_id.get(k).equals(TRUE_USER)) {

                                        MYFIRSTNAME = "Me";

                                    }
                                     // ------------------------------------------------------------------

                                    MessageItem messageItem = new MessageItem(isMe, text_content.get(k), first_name.get(k));  // create bubble

                                    messagesItems.add(messageItem); //add a bubble


                                    k++;}

                                // display list

                                adapter = new MessageAdapter(getActivity(), messagesItems);
                                setListAdapter(adapter);

                                messagesContainer.setSelection(negatif+NUMBER_MESSAGE_DISPLAY);
                                finish_loading = true;

                            } else if (nineMsgFirst == 0) {



                                messagesContainer.setSelection(1);
                                finish_loading = true;

                            } else {





                                int k = 0;
                                for (int i = nineMsgFirst; i < chatLineArgs; i++) {

                                    JSONObject jsonObjectCL = jsonArrayChatLine.getJSONObject(i);

                                    user_id.add(jsonObjectCL.getString("user_id"));  //get user_id of chat_line
                                    MEDIA_CONTENT = jsonObjectCL.getString("media_content");
                                    if (MEDIA_CONTENT.equals("null")) {

                                        text_content.add(jsonObjectCL.getString("text_content"));     //get the text content

                                    } else {
                                        MEDIA_CONTENT_SPLIT = MEDIA_CONTENT.substring(MEDIA_CONTENT.lastIndexOf("/") + 1);

                                        MEDIA_CONTENT_FINAL = "<a href=\"" + MEDIA_CONTENT + "\"> " + MEDIA_CONTENT_SPLIT + " </a>";


                                        try {

                                            MEDIA_CONTENT_DECODE = URLDecoder.decode(MEDIA_CONTENT_FINAL, "UTF-8");

                                        } catch (UnsupportedEncodingException e) {

                                            e.printStackTrace();

                                        }


                                        text_content.add(MEDIA_CONTENT_DECODE);
                                    }


                                    JSONObject jsonObjectUser = jsonObjectCL.getJSONObject("user"); //access to the user object

                                    if (!user_id.get(k).equals(TRUE_USER)) {

                                        first_name.add(jsonObjectUser.getString("first_name"));      //get the first name of each message

                                    } else {

                                        first_name.add("Me");
                                    }


                                    // To display messages on right side


                                    if (user_id.get(k).equals(TRUE_USER)) {

                                        isMe = true;


                                    } else if (!user_id.get(k).equals(TRUE_USER)) {

                                        isMe = false;
                                    }


                                    //--------------------- DISPLAY MY FIRSTNAME ----------------------

                                    if (user_id.get(k).equals(TRUE_USER)) {

                                        MYFIRSTNAME = "Me";

                                    }



                                    MessageItem messageItem = new MessageItem(isMe, text_content.get(k), first_name.get(k));  // create bubble

                                    messagesItems.add(messageItem); //add a bubble


                                    k++;
                                }

                                // display list


                                adapter = new MessageAdapter(getActivity(), messagesItems);
                                setListAdapter(adapter);


                                messagesContainer.setSelection(NUMBER_MESSAGE_DISPLAY - 1);


                            }


                        }


                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
            }
            }
        };

        //Error

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {



                AlertDialog.Builder builder = new AlertDialog.Builder(ctx,R.style.MyAlertDialogStyle);
                builder
                        .setMessage("Problem unknown")
                        .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(ctx, UserArea.class);
                                ctx.startActivity(intent);


                            }
                        })
                        .create()
                        .show();
            }
        };


        if (!finish_loading) {
            ChatRequest ChatRequest = new ChatRequest(token_dudu, "https://zenintra.net/chat_messages/" + id, listenerUpdate, errorListener);
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            requestQueue.add(ChatRequest);
        }


        return messagesItems;

    }

    public class ThreadGetMoreData extends Thread{


        @Override
        public void run() {
            //Add footer view after data

            mHandler.sendEmptyMessage(0);

            //search more data
            ArrayList<MessageItem> lstResult = getMoreData();

            //Delay time to show loading footer when debug, remove it when relase
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();

            }

            //send the result to handle
            Message msg = mHandler.obtainMessage(1,lstResult);
            mHandler.sendMessage(msg);


        }


    }






}
